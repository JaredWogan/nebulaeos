#!/bin/bash
#set -e

func_install() {
	if pacman -Qi $1 &> /dev/null; then
		tput setaf 2
  		echo "###############################################################################"
  		echo "################## The package "$1" is already installed"
      	echo "###############################################################################"
      	echo
		tput sgr0
	else
    	tput setaf 3
    	echo "###############################################################################"
    	echo "##################  Installing package "  $1
    	echo "###############################################################################"
    	echo
    	tput sgr0
    	sudo pacman -S --noconfirm --needed $1
    fi
}

###############################################################################
echo "Installation of the core software"
###############################################################################

list=(
sddm # Login manager
thunar # File explorer
thunar-archive-plugin # Archive manager
thunar-volman # Volume manager
xfce4-terminal # Thunar 'open with' terminal
autorandr # Automatic xrandr
awesome # Window manager
vicious # Awesome widgets
feh # Image Viewer
arcolinux-system-config # System config
arcolinux-wallpapers-git # Wallpapers
arcolinux-xfce-git # Xfce4 config
arcolinux-local-xfce4-git # Xfce4 local config
arcolinux-awesome-git # Awesome configs
arcolinux-dconf-all-desktops-git  # Configs
arcolinux-config-all-desktops-git # Configs
archlinux-logout-git # Logout
)

count=0

for name in "${list[@]}" ; do
	count=$[count+1]
	tput setaf 3;echo "Installing package nr.  "$count " " $name;tput sgr0;
	func_install $name
done

###############################################################################

tput setaf 6;echo "################################################################"
echo "Copying all files and folders from /etc/skel to ~"
echo "################################################################"
echo;tput sgr0
cp -Rf ~/.config ~/.config-backup-$(date +%Y.%m.%d-%H.%M.%S)
cp -arf /etc/skel/. ~

tput setaf 5;echo "################################################################"
echo "Enabling sddm as display manager"
echo "################################################################"
echo;tput sgr0
sudo systemctl enable sddm.service -f

tput setaf 7;echo "################################################################"
echo "You now have a very minimal functional desktop"
echo "################################################################"
echo;tput sgr0

tput setaf 11;
echo "################################################################"
echo "Reboot your system"
echo "################################################################"
echo;tput sgr0
