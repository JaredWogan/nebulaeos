# Enable custom NebulaeOS_Repo
file=/etc/pacman.conf

isNebulaeRepo=$(cat $file | grep -c "NebulaeOS_Repo")
if [ $isNebulaeRepo -eq 0 ]; then
   sudo echo "[NebulaeOS_Repo]" >> $file
   sudo echo "SigLevel = Optional TrustAll" >> $file
   sudo echo "Server = https://jaredwogan.ca/NebulaeOS_Repo/" >> $file
fi

# Make sure Parallel downloads are enabled
sudo sed -i 's/#ParallelDownloads/ParallelDownloads/g' $file

# Make sure the .config folder exists
config=~/.config/

if [ ! -d $config ]; then
   mkdir ~/.config/
fi